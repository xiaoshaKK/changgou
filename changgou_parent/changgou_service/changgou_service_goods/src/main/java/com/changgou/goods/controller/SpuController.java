package com.changgou.goods.controller;
import com.changgou.entity.PageResult;
import com.changgou.entity.Result;
import com.changgou.entity.StatusCode;
import com.changgou.goods.pojo.Goods;
import com.changgou.goods.service.SpuService;
import com.changgou.goods.pojo.Spu;
import com.github.pagehelper.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.List;
import java.util.Map;
@RestController
@CrossOrigin
@RequestMapping("/spu")
public class SpuController {


    @Autowired
    private SpuService spuService;

    /**
     * 查询全部数据
     * @return
     */
    @GetMapping
    public Result findAll(){
        List<Spu> spuList = spuService.findAll();
        return new Result(true, StatusCode.OK,"查询成功",spuList) ;
    }

    /***
     * 根据ID查询数据
     * @param id
     * @return
     */
    @GetMapping("/{id}")
    public Result findById(@PathVariable String id){
        Goods goods = spuService.findGoodsById(id);
        return new Result(true,StatusCode.OK,"查询成功",goods);
    }

    @GetMapping("/findSpuById/{id}")
    public Result<Spu> findSpuById(@PathVariable("id") String id){
        Spu spu = spuService.findById(id);
        return new Result(true,StatusCode.OK,"查询成功",spu);
    }


    /***
     * 新增数据
     *
     * 格式
     * **********************

     {
     "spu": {
     "name": "这个是商品名称",
     "caption": "这个是副标题",
     "brandId": 1115,
     "category1Id": 558,
     "category2Id": 559,
     "category3Id": 560,
     "freightId": 10,
     "image": "http://www.changgou.com/image/1.jpg",
     "images": "http://www.changgou.com/image/1.jpg,http://www.changgou.com/image/2.jpg",
     "introduction": "这个是商品详情，html代码",
     "paraItems": "{'出厂年份':'2019','赠品':'充电器'}",
     "saleService": "七天包退,闪电退货",
     "sn": "020102331",
     "specItems":  "{'颜色':['红','绿'],'机身内存':['64G','8G']}",
     "templateId": 42
     },
     "skuList": [{
     "sn": "10192010292",
     "num": 100,
     "alertNum": 20,
     "price": 900000,
     "spec": "{'颜色':'红','机身内存':'64G'}",
     "image": "http://www.changgou.com/image/1.jpg",
     "images": "http://www.changgou.com/image/1.jpg,http://www.changgou.com/image/2.jpg",
     "status": "1",
     "weight": 130
     },
     {
     "sn": "10192010293",
     "num": 100,
     "alertNum": 20,
     "price": 600000,
     "spec": "{'颜色':'蓝','机身内存':'128G'}",
     "image": "http://www.changgou.com/image/1.jpg",
     "images": "http://www.changgou.com/image/1.jpg,http://www.changgou.com/image/2.jpg",
     "status": "1",
     "weight": 130
     }
     ]
     }

     * *****************************************
     * @param goods
     * @return
     */
    @PostMapping
    public Result add(@RequestBody Goods goods){
        spuService.add(goods);
        return new Result(true,StatusCode.OK,"添加成功");
    }



    /***
     * 修改数据
     * @param goods
     * @param id
     * @return
     */
    @PutMapping(value="/{id}")
    public Result update(@RequestBody Goods goods,@PathVariable String id){
        // 我自己修改的
        goods.getSpu().setId(id);
        spuService.update(goods);
        return new Result(true,StatusCode.OK,"修改成功");
    }


    /***
     * 根据ID删除品牌数据
     * @param id
     * @return
     */
    @DeleteMapping(value = "/{id}" )
    public Result delete(@PathVariable String id){
        spuService.delete(id);
        return new Result(true,StatusCode.OK,"删除成功");
    }

    /**
     * 恢复数据
     * @param id
     * @return
     */
    @PutMapping("/restore/{id}")
    public Result restore(@PathVariable String id){
        spuService.restore(id);
        return new Result(true,StatusCode.OK,"恢复数据成功！");
    }

    /**
     * 物理删除
     * @param id
     * @return
     */
    @DeleteMapping("/realDelete/{id}")
    public Result realDelete(@PathVariable String id){
        spuService.realDelete(id);
        return new Result(true,StatusCode.OK,"物理删除成功！");
    }

    /***
     * 多条件搜索品牌数据
     * @param searchMap
     * @return
     */
    @GetMapping(value = "/search" )
    public Result findList(@RequestParam Map searchMap){
        List<Spu> list = spuService.findList(searchMap);
        return new Result(true,StatusCode.OK,"查询成功",list);
    }


    /***
     * 分页搜索实现
     * @param searchMap
     * @param page
     * @param size
     * @return
     */
    @GetMapping(value = "/search/{page}/{size}" )
    public Result findPage(@RequestParam Map searchMap, @PathVariable  int page, @PathVariable  int size){
        Page<Spu> pageList = spuService.findPage(searchMap, page, size);
        PageResult pageResult=new PageResult(pageList.getTotal(),pageList.getResult());
        return new Result(true,StatusCode.OK,"查询成功",pageResult);
    }


    /**
     * 审核
     * @param id
     * @return
     */
    @PutMapping("/audit/{id}")
    public Result audit(@PathVariable String id){
        spuService.audit(id);
        return new Result(true,StatusCode.OK,"商品审核成功！");
    }

    /**
     * 下架
     * @param id
     * @return
     */
    @PutMapping("/pull/{id}")
    public Result pull(@PathVariable String id){
        spuService.pull(id);
        return new Result(true,StatusCode.OK,"商品下架成功！");
    }

    /**
     * 上架
     * @param id
     * @return
     */
    @PutMapping("/put/{id}")
    public Result put(@PathVariable String id){
        spuService.put(id);
        return new Result(true,StatusCode.OK,"商品上架成功！");
    }

}
