package com.changgou.oauth;

import org.junit.Test;
import org.springframework.security.jwt.Jwt;
import org.springframework.security.jwt.JwtHelper;
import org.springframework.security.jwt.crypto.sign.RsaVerifier;

public class ParseJwtTest {
    /***
     * 校验令牌
     */
    @Test
    public void testParseToken(){
        //令牌
        String token = "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJzY29wZSI6WyJhcHAiXSwibmFtZSI6bnVsbCwiaWQiOm51bGwsImV4cCI6MTYyMTE5MTg5NCwiYXV0aG9yaXRpZXMiOlsiYWNjb3VudGFudCIsInVzZXIiLCJzYWxlc21hbiJdLCJqdGkiOiI5YzdiMDE3My1lZDk3LTQ3YTMtODUxZC1jYzA3OWRlNzhlYTciLCJjbGllbnRfaWQiOiJjaGFuZ2dvdSIsInVzZXJuYW1lIjoiaGVpbWEifQ.Rp8UCtkngu5lCcOuPJHq6KGlp2xXv8ANJvt7wKCvBU2E3mGkdKTHATS49cy_8AAZUcLz0nRx89bc51KEUU15pQWasHgjNdQGGciUEszXXy_E9gWQjcGTWIMrEQRZMhOYz8pCYbVS7I2AFRQAGMx3QM0wbH8HNgR9N2J7llC2BhGmT1Lqg56k_WBl9JlitDHEIb8-vFNl1t7qqBUOABG99ubgzwNM3ENO5sPIyAsnxxR-P6LeTKH9gmfw0tAWPWbZn5EZ1vJIFYUt95xzYNQFybioTU0GAZ8T8HGA4oYaui7u4_Go15Qqv7tNX_1mHhJP2vlAts2X6M_li-x16v137A";

        //公钥
        String publickey = "-----BEGIN PUBLIC KEY-----MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEApcl23FaBIdw2kSkYpFy99IWPu4hK/yWfkTHJcU7jqMli36shB71gTTkGjtRM6u7/7WJD1NNVEGMWcH2AS6wcbu8oO8nsEBY0M27Pah7SfmwZ7+iHnT6rQx/tJYDvhd1NmrSNlPi7cBuT8utS4ZaQcaiYjpZO+0X814hr3bUSJ6A6PbDerTkCvZ12Dyu3uSywE4ZmcTwlPUhbQOeZmtUXfqMZbJMS1TvLqCK65dGKWvtmm/C7X22nZRklSR795LRH8gq4kbNQPgSG+n+Q8RtnrKLBvz9/f4JSRJlW/ApMemCz6yse1L8T/Udx3qZPCBfARGpu6ptduvPQTsGfWSvJoQIDAQAB-----END PUBLIC KEY-----";

        //校验Jwt
        Jwt jwt = JwtHelper.decodeAndVerify(token, new RsaVerifier(publickey));

        //获取Jwt原始内容
        String claims = jwt.getClaims();
        System.out.println(claims);
        //jwt令牌
        String encoded = jwt.getEncoded();
        System.out.println(encoded);
    }
}
