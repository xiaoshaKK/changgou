linux安装openresty:

1.添加仓库执行命令

yum install yum-utils

yum-config-manager --add-repo https://openresty.org/package/centos/openresty.repo
2.执行安装

yum install openresty
3.安装成功后 会在默认的目录如下：

/usr/local/openresty


默认已经安装好了nginx,在目录：/usr/local/openresty/nginx 下。

修改/usr/local/openresty/nginx/conf/nginx.conf ,将配置文件使用的根设置为root,目的就是将来要使用lua脚本的时候 ，直接可以加载在root下的lua脚本。

#user nobody; 配置文件第一行原来为这样, 现改为下面的配置
user root root;


启动服务
service openresty start
设置自启动
systemctl enable openresty.service 

添加服务
location /ad_update {
	content_by_lua_file /root/lua/ad_update.lua;
}

location /ad_read {
	content_by_lua_file /root/lua/ad_read.lua;
}

#包含redis初始化模块
lua_shared_dict dis_cache 5m;  #共享内存开启


#限流设置
limit_req_zone $binary_remote_addr zone=myRateLimit:10m rate=2r/s;

    server {
        listen       8081;
        server_name  localhost;
        charset utf-8;
        location / {
            limit_req zone=myRateLimit;
            root   html;
            index  index.html index.htm;
        }
    }
# 突发请求
server {
    location / {
        limit_req zone=myRateLimit burst=5 nodelay;
        root   html;
        index  index.html index.htm;
    }
}


